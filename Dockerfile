# acme-tiny Docker image
#
# Copyright (C) 2022-2023 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.11.3-alpine3.17
FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='LSF operations team <ops@libre.space>'

ARG ACME_TINY_VERSION=5.0.1
ARG ACME_TINY_CONFIGDIR=/etc/acme-tiny.d
ARG ACME_TINY_VARSTATEDIR=/var/lib/acme-tiny

ENV PYTHONUNBUFFERED=1

# Install 'openssl'
RUN apk add --no-cache openssl

# Install 'acme-tiny'
RUN pip install --no-cache-dir acme-tiny

# Set acme-tiny cron job
RUN echo "0 4 * * * /usr/local/bin/acme-tiny.sh -c /etc/acme-tiny.d" > /etc/crontabs/root

# Create directories and volumes
RUN install -d \
	${ACME_TINY_CONFIGDIR} \
	${ACME_TINY_VARSTATEDIR}
VOLUME ["${ACME_TINY_CONFIGDIR}", "${ACME_TINY_VARSTATEDIR}"]

# Copy 'acme-tiny.sh' script
COPY acme-tiny.sh /usr/local/bin/

# Run crond
CMD ["crond", "-f", "-L", "/dev/stdout"]
